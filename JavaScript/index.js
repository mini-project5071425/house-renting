document.addEventListener('DOMContentLoaded', () => {
    const loadMoreButton = document.getElementById('loadMore');
    const propertiesSection = document.querySelector('.properties');
  
    // Fetch additional property data from a server or API
    function fetchPropertyData() {
      // Simulating fetching data
      const newProperties = [
        {
          image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTCVv69a_4v6WTLART7ovBSyyfcHZeIZNMevQ&s',
          title: 'Villa in Phnom Penh',
          description: 'Find a spacious villa for rent in Phnom Penh.',
          details: 'Room: 6 | $430 - $620'
        },
        {
          image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSk7SD-0R_l8QNyBeFxLnQE2bMxKCcekSp97g&s',
          title: 'Villa in Phnom Penh',
          description: 'Villa is the good location to rent in Phnom Penh.',
          details: 'Room: 5 | $501 - $637'
        },
        {
          image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS85ICpP6SGSUA0MppodD-wnDkkerSfVkdy_Q&s',
          title: 'Expat Rental : Phnom Penh',
          description: 'Find a house to rent in good life Phnom Penh.',
          details: 'Room: 3 | $500 - $1129'
        },
        {
          image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPHcbpUm4hUTJeKs-qs_5Uvl-Amm4Ut6Ekrw&s',
          title: 'Expat Rental : Phnom Penh',
          description: 'Find a house to rent in good life Phnom Penh.',
          details: 'Room: 3 | $500 - $1129'
        }
      ];
  
      return newProperties;
    }
  
    // Add new property cards to the page
    function displayNewProperties(properties) {
      properties.forEach(property => {
        const propertyCard = document.createElement('div');
        propertyCard.classList.add('property-card');
  
        propertyCard.innerHTML = `
          <img src="${property.image}" alt="${property.title}">
          <h3>${property.title}</h3>
          <p>${property.description}</p>
          <p>${property.details}</p>
          <a href="/HTML/learnmore.html" class="btn">Learn More</a>
        `;
  
        propertiesSection.appendChild(propertyCard);
      });
    }
  
    // Handle the "Load More" button click
    loadMoreButton.addEventListener('click', () => {
      const newProperties = fetchPropertyData();
      displayNewProperties(newProperties);
      loadMoreButton.style.display = 'none';
    });
  });
  const headerLink = document.getElementById('headerLink');
  headerLink.addEventListener('click', (event) => {
    event.preventDefault();
  });
  const learnMoreBtns = document.querySelectorAll('.learn-more');
  const cardDetails = document.querySelectorAll('.card-details');

  learnMoreBtns.forEach((btn, index) => {
    btn.addEventListener('click', () => {
      cardDetails[index].style.display = 'block';
      btn.style.display = 'none';
    });
  });